Billzone example module
=======================

This module helps understand the usage of the billzone module.

It contains a controller (BillzoneExampleController), which has 2 methods:
* createInvoice - This method contains all possible data, what you can pass to the Billzone system, when you want to create an invoice
* downloadInvoice - This method shows you how you can download the generated invoice and save as a file object